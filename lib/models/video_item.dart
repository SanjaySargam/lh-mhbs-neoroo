class VideoMetaData{
  final String name;
  final String url;
  final String image;
  final String description;
  VideoMetaData(
    {
      required this.description,
      required this.image,
      required this.name,
      required this.url,
    }
  );
}